<!DOCTYPE html>
<html>
    <head>
        <title>RoyalCMS :: Status</title>
        
        <meta charset="utf-8">
        
        <link rel="stylesheet" href="http://royalcms.net/css/general.css" />
        
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif-->
        
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-42733040-1', 'royalcms.net');
            ga('send', 'pageview');
        </script>
    </head>
    
    <body>
        <?php
        if ($_GET['error'] == 'no') {
            echo '<div style="background-color:green;width:100%;height:auto;color:white;font-size:21pt;padding-top:2px;padding-bottom:2px;text-align:center;">Your BETA form has been sent!</div>';
        } elseif ($_GET['error'] == 'yes') {
            echo '<div style="background-color:red;width:100%;height:auto;color:white;font-size:21pt;padding-top:2px;padding-bottom:2px;text-align:center;">Something wrong. Try again!</div>';
        }
        ?>
        <div id="header">
            <img src="http://royalcms.net/images/logo.png" alt="RoyalCMS logo" id="logo" />
        </div>
        
        <section id="content">
            <article>
                <header>
                    <h1>Want to see RoyalCMS in action?</h1>
                    <time class="tiny" pubdate datetime="2013-10-05 14:04">5 October 2013, 14:04</time><span class="tiny"> by Rick Lubbers</span>
                </header>
                <p>
                    <iframe width="640" height="480" src="//www.youtube-nocookie.com/embed/Cg706dIYGJ8?rel=0" frameborder="0" allowfullscreen></iframe>
                </p>
                <hr class="separator" />
            </article>
        
            <article>
                <header>
                    <h1>Say hello to RoyalCMS: Open Beta started!</h1>
                    <time class="tiny" pubdate datetime="2013-09-29 19:39">29 September 2013, 19:39</time><span class="tiny"> by Rick Lubbers</span>
                </header>
                <p>We have had some closed beta testers testing the system over the last weeks. And hey: we got some useful feedback. So we programmed and programmed until every tiny piece of advice had been implemented, one way or another. Now we think it is time to release RoyalCMS to you.</p>
                <p>Well, we have to admit something: we didn't create a downloadable version. That is, we made installation so simple you don't need one. Just go to <a href="https://extended.royalcms.net/">our store</a> (for lack of a better word). Register your account, and go to the Install section. You'll find all the information you need over there.</p>
                <p>Feedback can be submitted through our <a href="https://bitbucket.org/superice/royalcms/issues?status=new&status=open">issue tracker.</a> Just remember, we love feedback, so if you do come across a problem or you have a suggestion, do not hesitate creating an issue.</p>
                <hr class="separator" />
            </article>
        
            <article>
                <header>
                    <h1>Beta coming soon!</h1>
                    <time class="tiny" pubdate datetime="2013-08-04 11:36">4 August 2013, 11:36</time><span class="tiny"> by Rick Lubbers</span> / <time class="tiny" pubdate datetime="2013-08-12 13:00">12 August 2013, 13:00</time><span class="tiny"> updated by Maarten Braaksma</span>
                </header>
                <p>Over the last weeks we've drunk lots of coffee <i>(and Cola)</i>, so that means we got lots of programming done. The CMS is now almost ready for daily use, and RoyalExtended is almost done too. That means the only things left are documenting the whole thing for developers, and writing an upload-system to upload and manage plugins as developer.</p>
                <p>Since we don't mean to put a product with lots of bugs in it on the market, we need people to test it. If you think this is a cool initiative, if you want to support us, and if you've managed websites before, then contact us! (Contact details are in the bottom of the article) We need people like you. For the others: no panic. When betatesting is done, we'll release the code, and you'll be able to install it in no-time.</p>
                <p>Thank you for your support and cooperation!</p>
                <p>Edit 29-9-'13: as of today, beta is no longer closed. Just use the link above to install your copy of RoyalCMS</p>
                <hr class="separator" />
            </article>
            <article>
                <header>
                    <h1>RoyalCMS: a flexible content management system</h1>
                    <time class="tiny" pubdate datetime="2013-07-25 15:32">25 July 2013, 15:32</time><span class="tiny"> by Rick Lubbers</span>
                </header>
                <p>A few weeks ago, we came up with an idea for a content management system that is completely modular and easy to use. Let us explain it to you.</p>
                <p>Basically, the system is just a bunch of plugins bound together. Every piece of functionality is a plugin. That enables you to fully customize your system so it fits your needs. In other content management systems installing and using plugins is hard. Not in RoyalCMS. We have a plugin store, called RoyalExtended. Just search for plugins in the store, hit install, and the plugin is being installed in your system! It's supereasy to use, while it is fully customizable.</p>
                <p>Say you want to set up a blog. Just download and set up RoyalCMS, search for a blog plugin, and install it. Simple as that!</p>
                <p>RoyalCMS is also interesting for developers, since writing plugins is easy, and selling plugins is even more easy since we got RoyalExtended. It looks a bit like an app store for websites. Developers write plugins, we distribute them, and users install and use them. We seriously doubt we can make this any simpler.</p>
                <p>At the moment we're developing the software, and setting up RoyalExtended. We'll post updates here, and when it's ready, we'll put a download link here.</p>
                <hr class="separator" />
            </article>
        </section>
        
        <footer>
            &copy Copyright by RoyalCMS.net
        </footer>
    </body>
</html>
